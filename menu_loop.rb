require File.expand_path('catalog.rb', __dir__)

a = Catalog.new
a.add(Music.new(name: 'music_name', code: 1, size: 123, singer: 'Eminem', duration: 1243))
a.add(Movie.new(name: 'movie_name', code: 2, size: 123123, director: 'Dovzhenko',
                main_actor: 'Stupka', main_actress: 'Orkivna'))
a.add(Music.new(name: 'music_name', code: 3, size: 123, singer: 'Eminem', duration: 1423))
a.add(Movie.new(name: 'movie_name', code: 4, size: 123123, director: 'Dovzhenko',
                main_actor: 'Stupka', main_actress: 'Orkivna'))
a.add(Music.new(name: 'music_name', code: 5, size: 123, singer: 'Eminem', duration: 1253))
a.add(Movie.new(name: 'movie_name', code: 6, size: 123123, director: 'Dovzhenko',
                main_actor: 'Stupka', main_actress: 'Orkivna'))

menu_choose = 1

while menu_choose != 0
  puts "\n\nSelect option:\n1 - add new music\n
2 - add new movie\n
3 - show list media\n
4 - show by code\n
5 - remove by code\n
6 - play media #\n
0 - exit\n\n"
  menu_choose = gets.chomp.to_i
  case menu_choose
  when 1
    puts 'Add name: '
    new_name = gets.chomp
    puts 'Add code: '
    new_code = gets.chomp
    puts 'Add size: '
    new_size = gets.chomp
    puts 'Add singer: '
    new_singer = gets.chomp
    puts 'Add duration: '
    new_duration = gets.chomp
    a.add(Music.new(name: new_name, code: new_code, size: new_size, singer: new_singer,
                    duration: new_duration))
  when 2
    puts 'Add name: '
    new_name = gets.chomp
    puts 'Add code: '
    new_code = gets.chomp.to_i
    puts 'Add size: '
    new_size = gets.chomp
    puts 'Add director: '
    new_director = gets.chomp
    puts 'Add main actor: '
    new_main_actor = gets.chomp
    puts 'Add main actress: '
    new_main_actress = gets.chomp
    a.add(Movie.new(name: new_name, code: new_code, size: new_size, director: new_director,
                    main_actor: new_main_actor, main_actress: new_main_actress))
  when 3
    a.list
  when 4
    puts "\nEnter code what you want to see: "
    code = gets.chomp.to_i
    a.show(code)
  when 5
    puts "\nEnter code what you want to remove: "
    code = gets.chomp.to_i
    a.remove(code)
  when 6
    puts 'What media play: '
    play_code = gets.chomp.to_i
    a.items_list.each do |element|
      element.play_media if element.code == play_code
    end
    sleep 5
  when 0
    puts "\n\nThank you!\n\n"
    menu_choose = 0
  else
    puts "\n\nWrong option\n\n"
  end
end
