require File.expand_path('music.rb', __dir__)
require File.expand_path('movie.rb', __dir__)

class Catalog
  attr_reader :items_list

  def initialize
    @items_list = []
  end

  def add(item)
    items_list.push(item)
  end

  def remove(code)
    items_list.each_with_index do |element, index|
      items_list.delete_at(index) if element.code == code
    end
  end

  def show(code)
    find_item_by(code)
  end

  def list
    items_list.each(&:show_item)
  end

  private

  def find_item_by(code)
    items_list.each do |element|
      element.show_item if element.code == code
    end
  end
end
