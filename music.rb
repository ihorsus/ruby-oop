require File.expand_path('item.rb', __dir__)

class Music < Item
  CATEGORY = :music

  attr_accessor :singer, :duration

  def initialize(options = {})
    super
    @singer = options[:singer]
    @duration = options[:duration]
  end

  def category
    CATEGORY
  end

  def show_item
    puts "Name: '#{name}'"
    puts "Code: #{code}"
    puts "Category: #{CATEGORY}"
    puts "size: #{size} Kb"
    puts "Singer: '#{singer}'"
    puts "Duration: #{duration} sec"
    puts '---------------------------------------------'
  end

  def play_media
    puts "\n\nPlaying music ...\n\n"
  end
end
