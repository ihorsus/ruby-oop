require File.expand_path('item.rb', __dir__)

class Movie < Item
  CATEGORY = :movie

  attr_accessor :director, :main_actor, :main_actress

  def initialize(options = {})
    super
    @director = options[:director]
    @main_actor = options[:main_actor]
    @main_actress = options[:main_actress]
  end

  def category
    CATEGORY
  end

  def show_item
    puts "Name: '#{name}'"
    puts "Code: #{code}"
    puts "Category: #{CATEGORY}"
    puts "size: #{size} Kb"
    puts "Director: '#{director}'"
    puts "Main Actor: '#{main_actor}'"
    puts "Main Actress: '#{main_actress}'"
    puts '---------------------------------------------'
  end

  def play_media
    puts "\n\nPlaying movie ...\n\n"
  end
end
